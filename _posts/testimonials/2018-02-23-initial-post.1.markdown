---
title:  "Testimonial One"
date:   2018-02-23 21:00:02 +0000
categories: testimonials
---

{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}

